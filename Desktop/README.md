# Desktop Version

- read messages from gotify server (manually or every minute)
- delete single messages or all
- tray icon (sometimes suspecious disapper)
- displays base64 encoded images (e.g. from ImageSMS App)

## config

- look into `config.json.sample` to create your own `config.json`
- https needs the website cert. Export it from firefox and store it in `ca.crt`
- you have to add your own configs (ssl cert, url, client token) and recompile the desktop version.

## try it

- install nodejs (and npm)
- switch into the `Desktop/` folder
- do `npm i -D electron@latest`
- do `npm start`

## build

If you want to build your own desktop executable:

- install [yarn](https://yarnpkg.com)
- switch into the `Desktop/` folder
- do `npm install`
- do `yarn add electron-builder --dev`
- do `yarn dist`
