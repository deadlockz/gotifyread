var fs = require('fs');
var path = require('path'); 
var $ = require('jquery');
var https = require('https');
const { URL } = require('url');
const remote = require('electron').remote;
var nodeConsole = require('console');
var dateFormat = require('dateformat');

var conso = new nodeConsole.Console(process.stdout, process.stderr);
var logElement;
var config;
var ca;

var minime = function () {
    var window = remote.getCurrentWindow();
    window.close();
};

var deleteIt = function (mindex) {
    url = new URL(config.gotifyUrl + "/message/"+mindex+"?token=" + config.clientToken);
    https.request({
            hostname: url.hostname,
            port: url.port,
            path: url.pathname + url.search,
            method: 'DELETE',
            agent: new https.Agent({ca: ca})
        },
        (res) => {
            if (res.statusCode == 200) {
                $("#message"+mindex).hide(400);
            }
        }
    ).end(); // sync request. app is waiting
};

var deleteAll = function () {
    url = new URL(config.gotifyUrl + "/message?token=" + config.clientToken);
    https.request({
            hostname: url.hostname,
            port: url.port,
            path: url.pathname + url.search,
            method: 'DELETE',
            agent: new https.Agent({ca: ca})
        },
        (res) => {
            if (res.statusCode == 200) {
                logElement.html("");
                logElement.scrollTop = logElement.scrollHeight;
            }
        }
    ).end(); // sync request. app is waiting
};

var readThem = function () {
    https.get(
        config.gotifyUrl + "/message?token=" + config.clientToken,
        {
            agent: new https.Agent({ca: ca})
        }, 
        (res) => {
            var body = '';
            res.on('data', function(chunk) {
                body += chunk;
            });
            res.on('end', function() {
                json = JSON.parse(body);
                //conso.log(json);
                dummy  = '';
                for (var i = 0; i < json.messages.length; i++) {
                    dummy2 = json.messages[i].message;
                    if (i%2==0) {
                        dummy += '<div class="st" ';
                    } else {
                        dummy += '<div class="nd" ';
                    }
                    dummy += 'id="message' + json.messages[i].id + '">';
                    dummy += '<i>' + dateFormat(new Date(json.messages[i].date), 'dd.mm. HH:MM') + "</i>";
                    dummy += '<b>' + json.messages[i].title + "</b><br />";
                    if (dummy2.search('data:image/') > -1) {
                        pos = dummy2.search('data:image/');
                        dummy += dummy2.substr(0, pos);
                        dummy += '<br /><img src="';
                        dummy += dummy2.substr(pos).trim();
                        dummy += '">';
                    } else {
                        dummy += dummy2;
                    }
                    dummy +=  "<br />";
                    dummy += '<button class="del" onclick="deleteIt(' + json.messages[i].id + ');">&#9747;</button><br />';
                    dummy += "</div>";
                }
                logElement.html(dummy);
                logElement.scrollTop = logElement.scrollHeight;
            });
        }
    );
};

function intervalFunc() {
  readThem();
}

onload = function() {
    logElement = $("#output");
    config = JSON.parse(fs.readFileSync(path.join(__dirname, 'config.json'), 'utf8'));
    ca = fs.readFileSync(path.join(__dirname, 'ca.crt'), 'utf8');
    setInterval(intervalFunc, 60000);
};