package de.digisocken.gotifyread;

public class GotifyClient {
    public final String name;
    public final String description;

    public GotifyClient(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
