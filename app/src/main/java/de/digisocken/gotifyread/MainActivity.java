package de.digisocken.gotifyread;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Header;

public class MainActivity extends AppCompatActivity {
    public final static int MSGLIMIT = 80;

    // --------------------------------------------- ssl ignorance
    private boolean ignoreSSL = false;

    private String gotifyUrl = "";
    private String clientToken = "";

    private EditText gotifyEdit;
    private EditText gotifyToken;

    //private TextView txtView;
    private MsgAdapter msgAdapter;
    private ListView entryList;


    private SharedPreferences mPreferences;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        MenuItem ignoreItem = menu.findItem(R.id.actionIgnoreSSL);

        if (mPreferences.getBoolean("ignoreSSL", false)) {
            ignoreItem.setChecked(true);
            ignoreSSL = true;
        } else {
            ignoreItem.setChecked(false);
            ignoreSSL = false;
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionClient:
                Intent intClient = new Intent(this, LoginActivity.class);
                startActivityForResult(intClient, 4711);
                break;
            case R.id.actionIgnoreSSL:
                if (item.isChecked()) {
                    item.setChecked(false);
                    ignoreSSL = false;
                } else {
                    item.setChecked(true);
                    ignoreSSL = true;
                }
                mPreferences.edit().putBoolean("ignoreSSL", ignoreSSL).apply();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_AUTO);
            recreate();
        }
        setContentView(R.layout.activity_main);

        mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        entryList = (ListView) findViewById(R.id.listView);
        msgAdapter = new MsgAdapter(this, mPreferences);
        entryList.setAdapter(msgAdapter);


        gotifyEdit = (EditText) findViewById(R.id.gotifyEdit);
        gotifyToken = (EditText) findViewById(R.id.gotifyToken);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        if (!mPreferences.contains("ignoreSSL")) {
            mPreferences.edit().putBoolean("ignoreSSL", false).commit();
        }
        if (!mPreferences.contains("basicAuth")) {
            mPreferences.edit().putString("basicAuth", ":-S").commit();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mPreferences.contains("gotifyUrl")) {
            gotifyUrl = mPreferences.getString("gotifyUrl", gotifyUrl);
        }
        if (mPreferences.contains("clientToken")) {
            clientToken = mPreferences.getString("clientToken", clientToken);
        }
        if (!mPreferences.contains("ignoreSSL")) {
            ignoreSSL = mPreferences.getBoolean("ignoreSSL", false);
        }

        gotifyEdit.setText(gotifyUrl);
        gotifyToken.setText(clientToken);

        readThem(null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        gotifyUrl = gotifyEdit.getText().toString();
        clientToken = gotifyToken.getText().toString();

        mPreferences.edit().putString("gotifyUrl", gotifyUrl).apply();
        mPreferences.edit().putString("clientToken", clientToken).apply();
    }

    public void readThem(View view) {
        gotifyUrl = gotifyEdit.getText().toString();
        clientToken = gotifyToken.getText().toString();

        mPreferences.edit().putString("gotifyUrl", gotifyUrl).apply();
        mPreferences.edit().putString("clientToken", clientToken).apply();

        new getTask().execute();
    }

    private class clientTask extends AsyncTask<String, Void, GotifyClientResponse> {
        @Override
        protected GotifyClientResponse doInBackground(String... strings) {
            GotifyClientResponse response = null;

            if (!gotifyUrl.equals("")) {
                Retrofit retrofit = null;

                if ((gotifyUrl.startsWith("https://") && !ignoreSSL) || gotifyUrl.startsWith("http://")) {
                    retrofit = new Retrofit.Builder()
                            .baseUrl(gotifyUrl)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                } else {
                    // it is https and we ignore ssl certificate trouble !!!
                    URL destinationURL = null;
                    try {
                        destinationURL = new URL(gotifyUrl);
                        SslHelper.Basics basics = SslHelper.CertCheck(destinationURL);
                        OkHttpClient client = SslHelper.createOkClient(destinationURL, basics.certificate);
                        if (client == null) return response;

                        retrofit = new Retrofit.Builder()
                                .baseUrl(gotifyUrl)
                                .client(client)
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }

                GotifyService gms = retrofit.create(GotifyService.class);
                GotifyClient gotifyClient = new GotifyClient(strings[1], "a GotifyRead App");
                Call<GotifyClientResponse> call = gms.createClient(strings[0], gotifyClient);
                try {
                    //Log.v("xxxxxxxxxxxxxx", call.request().url().toString());
                    //Log.v("hhhhhh", call.request().header("authorization"));
                    response = call.execute().body();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return response;
        }

        @Override
        protected void onPostExecute(GotifyClientResponse response) {
            mPreferences.edit().putString("clientToken", response.token).apply();
            clientToken = response.token;
            gotifyToken.setText(clientToken);
        }
    }

    private class getTask extends AsyncTask<Void, Void, GotifyGetResponse> {
        @Override
        protected GotifyGetResponse doInBackground(Void... voids) {
            GotifyGetResponse response = null;

            if (!gotifyUrl.equals("") && !clientToken.equals("")) {
                Retrofit retrofit = null;

                if ((gotifyUrl.startsWith("https://") && !ignoreSSL) || gotifyUrl.startsWith("http://")) {
                    retrofit = new Retrofit.Builder()
                            .baseUrl(gotifyUrl)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                } else {
                    // it is https and we ignore ssl certificate trouble !!!
                    URL destinationURL = null;
                    try {
                        destinationURL = new URL(gotifyUrl);
                        SslHelper.Basics basics = SslHelper.CertCheck(destinationURL);
                        OkHttpClient client = SslHelper.createOkClient(destinationURL, basics.certificate);
                        if (client == null) return response;

                        retrofit = new Retrofit.Builder()
                                .baseUrl(gotifyUrl)
                                .client(client)
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }

                GotifyService gms = retrofit.create(GotifyService.class);

                Call<GotifyGetResponse> call = gms.get(clientToken, MSGLIMIT);
                try {
                    response = call.execute().body();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return response;
        }

        @Override
        protected void onPostExecute(GotifyGetResponse response) {
            msgAdapter.clear();
            if (response == null) return;
            for (GotifyMessage message : response.messages) {
                msgAdapter.addItem(message);
            }
            msgAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 4711) {
            if(resultCode == Activity.RESULT_OK){
                String client = data.getStringExtra("client_name");
                String basicAuth = data.getStringExtra("basicAuth");
                mPreferences.edit().putString("basicAuth", basicAuth).apply();
                new clientTask().execute(basicAuth, client);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                ;
            }
        }
    }
}
