package de.digisocken.gotifyread;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MsgAdapter extends BaseAdapter {
    public String squery;
    public ArrayList<GotifyMessage> msgEntries = new ArrayList<>();
    private Activity activity;
    private SharedPreferences mPreferences;

    MsgAdapter(Activity context, SharedPreferences preferences) {
        super();
        squery = null;
        activity = context;
        mPreferences = preferences;
    }

    public void addItem(GotifyMessage item) {
        msgEntries.add(item);
    }

    public void clear() {
        msgEntries.clear();
    }

    @Override
    public int getCount() {
        return msgEntries.size();
    }

    @Override
    public Object getItem(int i) {
        return msgEntries.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = activity.getLayoutInflater().inflate(R.layout.entry_line, viewGroup, false);
        TextView tt = (TextView) view.findViewById(R.id.line_title);
        final TextView tb = (TextView) view.findViewById(R.id.line_body);
        TextView td = (TextView) view.findViewById(R.id.line_date);

        tt.setText(msgEntries.get(i).title);
        td.setText(msgEntries.get(i).extractDate());

        SpannableStringBuilder ssb = null;
        Resources re = activity.getResources();
        ssb = msgEntries.get(i).extractImage(re);
        if (ssb != null) {
            tb.setText(ssb);
        } else {
            tb.setText(msgEntries.get(i).message);
        }

        if (i%2==0) {
            view.setBackgroundColor(ContextCompat.getColor(
                    activity.getApplicationContext(),
                    R.color.evenCol
            ));
        } else {
            view.setBackgroundColor(ContextCompat.getColor(
                    activity.getApplicationContext(),
                    R.color.oddCol
            ));
        }
        tb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PopupMenu popup = new PopupMenu(activity, tb);
                popup.getMenuInflater().inflate(R.menu.popup, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem mitem) {
                        int j = mitem.getItemId();
                        if (j == R.id.action_del){
                            new delTask().execute(i);
                            return true;
                        } else {
                            return onMenuItemClick(mitem);
                        }
                    }
                });

                popup.show();
            }
        });

        return view;
    }

    private class delTask extends AsyncTask<Integer, Void, Void> {
        private int index;
        private Call<Void> call;

        @Override
        protected Void doInBackground(Integer... ints) {
            index = ints[0];

            String gotifyUrl = mPreferences.getString("gotifyUrl", "");
            String clientToken = mPreferences.getString("clientToken", "");
            Boolean ignoreSSL = mPreferences.getBoolean("ignoreSSL", false);

            if (!gotifyUrl.equals("") && !clientToken.equals("")) {
                Retrofit retrofit = null;

                if ((gotifyUrl.startsWith("https://") && !ignoreSSL) || gotifyUrl.startsWith("http://")) {
                    retrofit = new Retrofit.Builder()
                            .baseUrl(gotifyUrl)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                } else {
                    // it is https and we ignore ssl certificate trouble !!!
                    URL destinationURL = null;
                    try {
                        destinationURL = new URL(gotifyUrl);
                        SslHelper.Basics basics = SslHelper.CertCheck(destinationURL);
                        OkHttpClient client = SslHelper.createOkClient(destinationURL, basics.certificate);
                        if (client == null) return null;

                        retrofit = new Retrofit.Builder()
                                .baseUrl(gotifyUrl)
                                .client(client)
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }

                GotifyService gms = retrofit.create(GotifyService.class);
                call = gms.del(clientToken, msgEntries.get(index).id);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (call != null) {
                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, retrofit2.Response<Void> response) {
                        if (response.code() == 200) {
                            msgEntries.remove(index);
                            notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) { }
                });
            }
        }
    }
}
