package de.digisocken.gotifyread;

public class GotifyPaging {

    public final int limit;
    public final String next;
    public final int since;
    public final int size;

    public GotifyPaging(int limit, String next, int since, int size) {
        this.limit = limit;
        this.next = next;
        this.since = since;
        this.size = size;
    }
}