package de.digisocken.gotifyread;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GotifyService {

    @POST("/message")
    Call<GotifyMessage> push(
            @Query("token") String clientToken,
            @Body GotifyMessage gotifyMessage
    );

    @POST("/client")
    Call<GotifyClientResponse> createClient(
            @Header("authorization") String basicAuth,
            @Body GotifyClient gotifyClient
    );

    @GET("/message")
    Call<GotifyGetResponse> get(
            @Header("X-Gotify-Key") String token,
            @Query("limit") int limit
    );

    @DELETE("/message/{index}")
    Call<Void> del(
            @Header("X-Gotify-Key") String token,
            @Path("index") int index
    );
}
