package de.digisocken.gotifyread;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {
    private EditText clientEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_AUTO);
            recreate();
        }
        setContentView(R.layout.popup_input_dialog);
        clientEdit = (EditText) findViewById(R.id.clientName);
        clientEdit.setText(android.os.Build.MODEL);
    }

    public void clickCancel(View view) {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    public void clickOk(View view) {
        Intent returnIntent = new Intent();
        String user = ((EditText) findViewById(R.id.userEdit)).getText().toString();
        String pass = ((EditText) findViewById(R.id.passwordEdit)).getText().toString();
        String client = clientEdit.getText().toString();

        byte[] bytes = (user + ":" + pass).getBytes();
        String basicAuth = "Basic " + Base64.encodeToString(bytes, Base64.NO_WRAP);
        returnIntent.putExtra("basicAuth", basicAuth);
        returnIntent.putExtra("client_name", client);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
