package de.digisocken.gotifyread;

import java.util.List;

public class GotifyGetResponse {
    public final List<GotifyMessage> messages;
    public final GotifyPaging paging;

    public GotifyGetResponse(List<GotifyMessage> messages, GotifyPaging paging) {
        this.messages = messages;
        this.paging = paging;
    }
}
