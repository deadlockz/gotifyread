package de.digisocken.gotifyread;

class GotifyClientResponse {
    public final int id;
    public final String name;
    public final String token;

    public GotifyClientResponse(int id, String name, String token) {
        this.id = id;
        this.token = token;
        this.name = name;
    }
}
