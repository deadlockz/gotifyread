package de.digisocken.gotifyread;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Base64;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class GotifyMessage {
    public final String date;
    public final int priority;
    public final int id;
    public final String title;
    public final String message;

    public GotifyMessage(int priority, String title, String message) {
        this.priority = priority;
        this.title = title;
        this.message = message;
        this.date = null;
        this.id = -1;
    }

    public GotifyMessage(int priority, String title, String message, String date, int id) {
        this.priority = priority;
        this.title = title;
        this.message = message;
        this.date = date;
        this.id = id;
    }

    @Override
    public String toString() {
        return String.format(
                Locale.ENGLISH,
                "(%d) %s %s\n%s\n\n",
                id,
                extractDate(),
                title,
                message
        );
    }

    public String extractDate() {
        String dat = date.substring(0, date.lastIndexOf("."));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.GERMAN);
        // X not working on old API
        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSXXX", Locale.ENGLISH);
        try {
            Date ddate = simpleDateFormat.parse(dat);
            dat = new SimpleDateFormat("dd.MM. HH:mm").format(ddate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dat;
    }

    public SpannableStringBuilder extractImage(Resources re) {
        if (!this.message.contains("data:image/")) return null;
        CharSequence sp = this.message.substring(0, this.message.indexOf("data:image/")) + "\n  \n";
        SpannableStringBuilder ssb = new SpannableStringBuilder(sp);
        String coded = this.message.substring(this.message.indexOf(";base64,") + 8);
        byte[] decodedString;

        try {
            decodedString = Base64.decode(coded, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            if (decodedByte == null) return null;
            Drawable dr = new BitmapDrawable(re, decodedByte);
            dr.setBounds(
                    0,
                    0,
                    decodedByte.getWidth(),
                    decodedByte.getHeight()
            );

            ImageSpan isp = new ImageSpan(dr);
            ssb.setSpan(
                    isp,
                    sp.length()-3,
                    sp.length()-2,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE
            );
            return ssb;
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
