# Gotify Read

![Layout](app/src/main/res/mipmap-xxhdpi/ic_launcher.png)

Gotify Read is very low level Gotify Client for Android and [OS X, Win, Linux Desktop](Desktop/README.md) just to read and delete text messages.

The App is able to display base64 encoded images (e.g. from my ImageSMS App). Automatic night theme is added, too.

Delete: tab on the message body to get a popup menu!

## Screenshot

![Screenshot](picture.png)

## Screenshot Desktop Version

![Screenshot Desktop](picture-desktop.png)

you have to add your own configs (ssl cert, url, client token) and recompile the desktop version.

## Access to Gotify from eduroam

Here is a list of ports (use http), where eduroam accept connections to:

![firewall holes](eduroam_ok_ports.png)

## Get the App

- for Android 4.0+ [APK file](https://gitlab.com/deadlockz/gotifyread/raw/master/app/release/de.digisocken.gotifyread.apk)

## License

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or distribute this 
software, either in source code form or as a compiled binary, for any purpose, 
commercial or non-commercial, and by any means.

In jurisdictions that recognize copyright laws, the author or authors of this software 
dedicate any and all copyright interest in the software to the public domain. We make 
this dedication for the benefit of the public at large and to the detriment of our 
heirs and successors. We intend this dedication to be an overt act of relinquishment 
in perpetuity of all present and future rights to this software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE.

For more information, please refer to [http://unlicense.org](http://unlicense.org)

## Privacy Policy

### Personal information.

Personal information is data that can be used to uniquely identify or contact a
single person. I DO NOT collect, transmit, store or use any personal information while you use this app.

### Non-Personal information.

I DO NOT collect non-personal information like user's behavior:

 -  to solve App problems
 -  to show personalized ads

The Google Play Store collects non-personal information such as the data of install (country and equipment).
I did not add any Google or ad keys or codes for marketing feedback, Ads or payment systems!

### Privacy Questions.

If you have any questions or concerns about my Privacy Policy or data processing, please contact me.
